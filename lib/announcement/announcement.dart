import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_renyi/common/common.dart';
import 'package:flutter_renyi/common/toast.dart';
import 'package:flutter_renyi/model/article_announcement.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_renyi/api/apiRequest.dart';

class Announcement extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AnnouncementStatePage();
  }
}

class AnnouncementStatePage extends State<Announcement>{

  RefreshController _refreshController =
  RefreshController(initialRefresh: true);

  int _page = 1;

  List<ArticleAnnouncement> articleList = [];

  @override
  void initState() {
    super.initState();
    // _onRefresh();
  }


  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    await apiRequest
        .request('/collection_newslist', method: apiRequest.GET, data: {
      'page': _page.toString(),
    }).then((value) {
      if (value['collectionlist'].length > 0) {
        setState(() {
          List alist = value['collectionlist'];

          articleList = alist.map((dynamic json) {
            return ArticleAnnouncement.fromJson(json);
          }).toList();
        });
      }else{
        _refreshController.loadNoData();
      }
    });

    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    //print('上拉刷新');
    _page++;
    await apiRequest
        .request('/collection_newslist', method: apiRequest.GET, data: {
      'page': _page.toString(),
    }).then((value) {
      if (value['collectionlist'].length > 0) {
        setState(() {
          List alist = value['collectionlist'];
          articleList.addAll(alist.map((dynamic json) => ArticleAnnouncement.fromJson(json)).toList());
        });
      } else {
        //print('没有数据啦');
        Toast.show('没有数据啦');
        _refreshController.loadNoData();
      }
    });
    _refreshController.loadComplete();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('公告'),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '刷新完成',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            // if(mode==LoadStatus.idle){
            //   //body =  Text("上拉加载");
            // }
            if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else if (mode == LoadStatus.noMore) {
              body = Text('没有数据啦');
            }
            return Container(
              height: 30.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        
        child: ListView.builder(
            itemCount: articleList.length,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.all(8),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(articleList[index]?.title ?? '',style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                    ),

                    Row(
                      children: [
                        Container(
                          width: 130,
                          height: 110,
                          child: Image.network(articleList[index]?.thumb ?? ''),
                        ),
                        Expanded(
                          child: Container(
                            //color: Colors.red,
                            height: 95,
                            margin: EdgeInsets.only(left: 8),
                            child: Text(articleList[index]?.description ?? '',
                              maxLines: 4,overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: 13),
                            ),
                          )
                        ),

                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Row(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                width: 40,
                                decoration: BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.red, //边框颜色
                                    width: 1, //边框宽度
                                  ), // 边色与边宽度
                                  color: Colors.white, // 底色
                                  // borderRadius: BorderRadius.circular(10), // 圆角也可控件一边圆角大小
                                  borderRadius: BorderRadius.all(Radius.circular(2)),
                                ),
                                child: Text('咨询'),
                              ),

                              SizedBox(width: 4,),
                              Text(Constant.getData(articleList[index]?.publishTime ?? '')),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: [
                              Text(articleList[index]?.click.toString() ?? ''),
                              SizedBox(width: 1,),
                              Text('阅读')
                            ],
                          ),
                        )

                      ],
                    )
                  ],
                ),
              );
            }
        ),
      )
    );
  }
  

}