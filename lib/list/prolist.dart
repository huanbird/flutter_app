import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_renyi/api/apiRequest.dart';
import 'package:flutter_renyi/common/common.dart';
import 'package:flutter_renyi/model/pro_list.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

String ossHeader = 'http://renyipublic.oss-cn-shanghai.aliyuncs.com/';
String ossSuffix =
    '?x-oss-process=image/resize,m_fill,h_600,w_600/sharpen,100,t_90,g_se,x_10,y_35,color_808080,image/bright,10';

class ProList extends StatefulWidget {
  const ProList({Key key, this.type}) : super(key: key);

  final int type;

  @override
  State<StatefulWidget> createState() {
    return ListRes();
  }
}

class ListRes extends State<ProList> {

  //final FocusNode focusNode = FocusNode();//点击空白关闭键盘
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  List<ProListRes> proList = [];

  int _page = 1;
  int _index = 0;

  String _AppTitle = '';

  @override
  void initState() {
    if (widget.type == 1) {
      _AppTitle = '交易列表';
    }else if(widget.type == 2){
      _AppTitle = '艺海识臻';
    }
    super.initState();
    // _onRefresh();
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if(widget.type == 1){
      await apiRequest
          .request('/goods_list_by_chinese', method: apiRequest.GET, data: {
        'page': _page.toString(),
      }).then((value) {
        if (value['goods_list'].length > 0) {
          setState(() {
            List alist = value['goods_list'];

            proList = alist.map((dynamic json) {
              return ProListRes.fromJson(json);
            }).toList();
          });
        }
      });
    }else if(widget.type == 2){
      await apiRequest
          .request('/iscommid', method: apiRequest.GET, data: {
        'page': _page.toString(),
      }).then((value) {
        if (value['goods_list'].length > 0) {
          setState(() {
            List alist = value['goods_list'];

            proList = alist.map((dynamic json) {
              return ProListRes.fromJson(json);
            }).toList();
          });
        }else{
          _refreshController.loadNoData();
        }
      });
    }

    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    //print('上拉刷新');
    _page++;
    await apiRequest
        .request('/goods_list_by_chinese', method: apiRequest.GET, data: {
      'page': _page.toString(),
    }).then((value) {
      if (value['goods_list'].length > 0) {
        setState(() {
          //res.map((dynamic json) => HomeBanner.fromJson(json)).toList();
          List alist = value['goods_list'];
          proList.addAll(
              alist.map((dynamic json) => ProListRes.fromJson(json)).toList());
        });
      } else {
        _refreshController.loadNoData();
      }
    });
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.type == 1 ? TopSearchBar(context, _AppTitle) : AppBar(title: Text(_AppTitle),),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '刷新完成',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            // if(mode==LoadStatus.idle){
            //   //body =  Text("上拉加载");
            // }
            if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else if (mode == LoadStatus.noMore) {
              body = Text('没有数据啦');
            }
            return Container(
              height: 30.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: GridView.builder(
          padding: EdgeInsets.all(3),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 2 / 2.8,
          ),
          itemBuilder: (context, index) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Color.fromRGBO(233, 233, 233, 0.9), width: 1)),
              //height: 300,
              //color: Colors.primaries[index % Colors.primaries.length],
              child: Column(
                children: <Widget>[
                  SizedBox(height: 5),
                  Image.network(
                    ossHeader + proList[index].originalImg + ossSuffix,
                    height: 170,
                    width: 170,
                    //fit: BoxFit.cover,
                  ),
                  SizedBox(height: 10),
                  Container(
                    alignment: AlignmentDirectional.centerStart,
                    child: Text(
                      proList[index].goodsName,
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(height: 5),
                  Container(
                    padding: EdgeInsets.only(left: 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('¥' + proList[index].curPrice,
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: Colors.deepOrange)),
                        Row(
                          children: <Widget>[
                            Image.network(
                              'http://wap.luckywen.com/static/img/glance.0f1c7b8.png',
                              width: 20,
                              height: 25,
                            ),
                            Text(proList[index].clickCount.toString())
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
          itemCount: proList.length,
        ),
      ),
    );
  }
}

Widget TopSearchBar(BuildContext context, String _AppTitle) {
  //
  return PreferredSize(
    preferredSize: Size.fromHeight(120),
    child: Column(
      children: [
        AppBar(
          title: Text(_AppTitle),
        ),
        Container(
            color: Color(0xffC2A993),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    color: Colors.white,
                    margin:
                        EdgeInsets.only(left: 10, top: 5, right: 4, bottom: 5),
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        hintText: '搜索您想要的商品',
                        hintStyle: TextStyle(color: Colors.grey),
                        hintMaxLines: 1,
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 60,
                  height: 48,
                  margin:
                  EdgeInsets.only(left: 2, top: 0, right: 10, bottom: 0),
                  child: RaisedButton(
                    color: Colors.white,
                      child: Text(
                        '搜索',
                        style: TextStyle(color: Color(0xffF30A00),fontSize: 14),
                      ),
                      onPressed: () {
                        print('sssssss');
                      }),
                ),
              ],
            ))
        // Row(
        //   children: [
        //
        //     Container(
        //       width: 50,
        //       height: 60,
        //       color: Color(0xffC2A993),
        //       child: RaisedButton(
        //           child: Text(
        //             '搜索',
        //             style: TextStyle(color: Color(0xffF30A00)),
        //           ),
        //           onPressed: () {
        //             print('sssssss');
        //           }),
        //     ),
        //
        //   ],
        // )
      ],
    ),
  );
}

Widget list(context, index) {
  return Container(
    child: Text('列表'),
  );
}
