import 'package:fluro/fluro.dart';
import '../routers/router_init.dart';
import './prolist.dart';


class ProListRoute implements IRouterProvider{

  static String proListPage = '/pro_list';
  static String shopSettingPage = '/shop/shopSetting';


  @override
  void initRouter(Router router) {
    router.define(proListPage, handler: Handler(handlerFunc: (_, params) {
      final int index = int.parse(params['type']?.first);
      return ProList(type: index,);
    }));
  }

}