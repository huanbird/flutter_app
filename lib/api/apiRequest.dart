import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'dart:async';

import 'package:oktoast/oktoast.dart';

/*
 * 封装 restful 请求
 *
 * GET、POST、DELETE、PATCH
 * 主要作用为统一处理相关事务：
 *  - 统一处理请求前缀；
 *  - 统一打印请求信息；
 *  - 统一打印响应信息；
 *  - 统一打印报错信息；
 */
class apiRequest {

  /// global dio object
  static Dio dio;

  /// default options
  static const String API_PREFIX = 'http://api.luckywen.com';
  static const int CONNECT_TIMEOUT = 10000;
  static const int RECEIVE_TIMEOUT = 3000;

  /// http request methods
  static const String GET = 'get';
  static const String POST = 'post';
  static const String PUT = 'put';
  static const String PATCH = 'patch';
  static const String DELETE = 'delete';

  static const String TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIiwianRpIjoiNGYxZzIzZnNmYWFhYTEyYWEifQ.eyJpc3MiOiJodHRwOlwvXC93LnNpaml3ZWlwYWkuY29tIiwiYXVkIjoiaHR0cDpcL1wvdy5zaWppd2VpcGFpLmNvbSIsImp0aSI6IjRmMWcyM2ZzZmFhYWExMmFhIiwiaWF0IjoxNTk3ODkxMzg0LCJuYmYiOjE1OTc4OTEzODQsImV4cCI6MTU5Nzk3Nzc4NCwidXNlcl9pZCI6MSwic3Vic2NyaWJlIjoxLCJoZWFkX3BpYyI6Imh0dHA6XC9cL3RoaXJkd3gucWxvZ28uY25cL21tb3BlblwvYWpOVmRxSFpMTEN0M0p3VHYzVFVBcmlhOUQzRW1waEJlbjcxZTF5V3FzSmljMjJYVWlibm4zcjFxeG1rdGdWSG5lNEVOd3dndGVXaWF3Z1VCeUFNbnhGZ2JMRnJITEhTaWFCaWFvNGlhQ0YxQmVwTDFJXC8xMzIiLCJuaWNrbmFtZSI6Imh1YW5iaXJkIiwib3BlbmlkIjoibzhyR3MxWEllMzl4akFSWk1EeVN3c1Z3V2lNQSJ9.';

  /// request method
  static Future<Map> request (
      String url,
      { data, method }) async {
    //data = {"id" : 1,"ss":2};
    method = method ?? 'GET';
    data = data ?? {};
    data['token'] = TOKEN;

    /// 打印请求相关信息：请求地址、请求方式、请求参数
    print('请求地址：【' + method + '  ' + url + '】');
    print('请求参数：' + data.toString());

    Dio dio = createInstance();
    var result;

    try {
      Response response = await dio.request(url, queryParameters: data, options: new Options(method: method));

      if (response.statusCode == 200){
        print(response.data is String);
        result = response.data['data'];
        print('请求响应：' + result.toString());
//        print(response.data is String);

      }

    } on DioError catch (e) {
      showToast('请求失败');
      /// 打印请求失败相关信息
      print('请求出错：' + e.toString());
    }

    return result;
  }


  /// request method
  static Future<Map> requestHome (
      String url,
      { data, method }) async {
    //data = {"id" : 1,"ss":2};
//    method = method ?? 'GET';
//    data = data ?? {};
//    print(data);
//    data['token'] = TOKEN;
    /// 打印请求相关信息：请求地址、请求方式、请求参数
    print('请求地址 home：【' + method + '  ' + url + '】');
    //print('请求参数 home：' + data.toString());

    Dio dio = createInstance();
    var result;

    try {
      Response response = await dio.request(url, data: data, options: new Options(method: method));

      if (response.statusCode == 200){
        result = response.data;
      }

    } on DioError catch (e) {
      /// 打印请求失败相关信息
      print('请求出错：' + e.toString());
    }

    return result;
  }

  /// 创建 dio 实例对象
  static Dio createInstance () {
    if (dio == null) {
      /// 全局属性：请求前缀、连接超时时间、响应超时时间
      var options = BaseOptions(
        baseUrl: API_PREFIX,
        connectTimeout: CONNECT_TIMEOUT, //30s
        receiveTimeout: RECEIVE_TIMEOUT, //30s
        headers: {
          //'platform': '${Platform.operatingSystem}',
          //'Authorization': 'Bearer $token',
          //'version': '${packageInfo?.version}',
          //'versionCode': '${packageInfo?.buildNumber}',
        },
      );

      dio = new Dio(options);
    }

    return dio;
  }

  /// 清空 dio 对象
  static clear () {
    dio = null;
  }

}