import 'package:json_annotation/json_annotation.dart';

part 'trans_actions.g.dart';
@JsonSerializable()
class TransActions {
  @JsonKey(name: 'cat_desc')
  String catDesc;
  @JsonKey(name: 'engish_name')
  String engIshName;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'mobile_name')
  String mobileName;
  @JsonKey(name: 'image')
  String image;
  @JsonKey(name: 'right_image')
  String rightImage;
  @JsonKey(name: 'id')
  int id;
  TransActions({this.catDesc,this.engIshName,this.name,this.mobileName,this.image,this.rightImage,this.id});

  factory TransActions.fromJson(Map<String, dynamic> json) => _$TransActionsFromJson(json);

  Map<String, dynamic> toJson() => _$TransActionsToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}