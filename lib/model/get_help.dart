import 'package:json_annotation/json_annotation.dart';
part 'get_help.g.dart';
@JsonSerializable()
class GetHelp {
  @JsonKey(name: 'image')
  String image;
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'parent_id')
  int parentId;

  GetHelp({this.image,this.id,this.parentId});

  factory GetHelp.fromJson(Map<String, dynamic> json) => _$GetHelpFromJson(json);

  Map<String, dynamic> toJson() => _$GetHelpToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }

}