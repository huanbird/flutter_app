// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trans_actions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransActions _$TransActionsFromJson(Map<String, dynamic> json) {
  return TransActions(
    catDesc: json['cat_desc'] as String,
    engIshName: json['engish_name'] as String,
    name: json['name'] as String,
    mobileName: json['mobile_name'] as String,
    image: json['image'] as String,
    rightImage: json['right_image'] as String,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$TransActionsToJson(TransActions instance) =>
    <String, dynamic>{
      'cat_desc': instance.catDesc,
      'engish_name': instance.engIshName,
      'name': instance.name,
      'mobile_name': instance.mobileName,
      'image': instance.image,
      'right_image': instance.rightImage,
      'id': instance.id,
    };
