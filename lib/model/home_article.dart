import 'package:json_annotation/json_annotation.dart';

part 'home_article.g.dart';

@JsonSerializable()
class HomeArticle {
  @JsonKey(name: 'add_time')
  String addTime;
  @JsonKey(name: 'article_id')
  int articleId;
  @JsonKey(name: 'click')
  int click;
  @JsonKey(name: 'description')
  String description;
  @JsonKey(name: 'publish_time')
  int publishTime;
  @JsonKey(name: 'thumb')
  String thumb;
  @JsonKey(name: 'title')
  String title;

  HomeArticle({
    this.addTime,
    this.articleId,
    this.click,
    this.description,
    this.publishTime,
    this.thumb,
    this.title,
  });

  factory HomeArticle.fromJson(Map<String, dynamic> json) =>
      _$HomeArticleFromJson(json);

  Map<String, dynamic> toJson() => _$HomeArticleToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}
