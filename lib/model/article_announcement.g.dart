// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_announcement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticleAnnouncement _$ArticleAnnouncementFromJson(Map<String, dynamic> json) {
  return ArticleAnnouncement(
    addTime: json['add_time'] as int,
    articleId: json['article_id'] as int,
    author: json['author'] as String,
    click: json['click'] as int,
    description: json['description'] as String,
    publishTime: json['publish_time'] as int,
    thumb: json['thumb'] as String,
    title: json['title'] as String,
  );
}

Map<String, dynamic> _$ArticleAnnouncementToJson(
        ArticleAnnouncement instance) =>
    <String, dynamic>{
      'add_time': instance.addTime,
      'article_id': instance.articleId,
      'author': instance.author,
      'click': instance.click,
      'description': instance.description,
      'publish_time': instance.publishTime,
      'thumb': instance.thumb,
      'title': instance.title,
    };
