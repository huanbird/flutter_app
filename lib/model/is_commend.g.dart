// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'is_commend.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IsCommend _$IsCommendFromJson(Map<String, dynamic> json) {
  return IsCommend(
    name: json['name'] as String,
    addTime: json['add_time'] as String,
    image: json['image'] as String,
  );
}

Map<String, dynamic> _$IsCommendToJson(IsCommend instance) => <String, dynamic>{
      'name': instance.name,
      'add_time': instance.addTime,
      'image': instance.image,
    };
