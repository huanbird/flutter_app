// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_help.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetHelp _$GetHelpFromJson(Map<String, dynamic> json) {
  return GetHelp(
    image: json['image'] as String,
    id: json['id'] as int,
    parentId: json['parent_id'] as int,
  );
}

Map<String, dynamic> _$GetHelpToJson(GetHelp instance) => <String, dynamic>{
      'image': instance.image,
      'id': instance.id,
      'parent_id': instance.parentId,
    };
