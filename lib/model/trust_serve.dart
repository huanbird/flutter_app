import 'package:json_annotation/json_annotation.dart';
part 'trust_serve.g.dart';


@JsonSerializable()
class TrustServe {
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'mobile_name')
  String mobileName;
  @JsonKey(name: 'image')
  String image;
  @JsonKey(name: 'id')
  int id;
  TrustServe({this.name,this.mobileName,this.image,this.id});

  factory TrustServe.fromJson(Map<String, dynamic> json) => _$TrustServeFromJson(json);

  Map<String, dynamic> toJson() => _$TrustServeToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }

}