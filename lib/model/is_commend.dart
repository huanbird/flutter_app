import 'package:json_annotation/json_annotation.dart';
part 'is_commend.g.dart';
@JsonSerializable()
class IsCommend {
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'add_time')
  String addTime;
  @JsonKey(name: 'image')
  String image;

  IsCommend({this.name, this.addTime, this.image});
  factory IsCommend.fromJson(Map<String, dynamic> json) =>
      _$IsCommendFromJson(json);

  Map<String, dynamic> toJson() => _$IsCommendToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}
