// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pro_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProListRes _$ProListResFromJson(Map<String, dynamic> json) {
  return ProListRes(
    goodsId: json['goods_id'] as int,
    goodsName: json['goods_name'] as String,
    careCount: json['care_count'] as int,
    clickCount: json['click_count'] as int,
    curPrice: json['cur_price'] as String,
    endTime: json['endTime'] as int,
    goodsStatus: json['goods_status'] as int,
    isCare: json['is_care'] == null
        ? null
        : Is_care.fromJson(json['is_care'] as Map<String, dynamic>),
    originalImg: json['original_img'] as String,
    shopPrice: json['shop_price'] as String,
    startPrice: json['start_price'] as String,
  );
}

Map<String, dynamic> _$ProListResToJson(ProListRes instance) =>
    <String, dynamic>{
      'care_count': instance.careCount,
      'click_count': instance.clickCount,
      'cur_price': instance.curPrice,
      'endTime': instance.endTime,
      'goods_id': instance.goodsId,
      'goods_name': instance.goodsName,
      'goods_status': instance.goodsStatus,
      'is_care': instance.isCare,
      'original_img': instance.originalImg,
      'shop_price': instance.shopPrice,
      'start_price': instance.startPrice,
    };

Is_care _$Is_careFromJson(Map<String, dynamic> json) {
  return Is_care(
    json['code'] as int,
    json['msg'] as String,
  );
}

Map<String, dynamic> _$Is_careToJson(Is_care instance) => <String, dynamic>{
      'code': instance.code,
      'msg': instance.msg,
    };
