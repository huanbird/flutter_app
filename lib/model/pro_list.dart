import 'package:json_annotation/json_annotation.dart';

part 'pro_list.g.dart';

@JsonSerializable()

class ProListRes {
  @JsonKey(name: 'care_count')
  int careCount;

  @JsonKey(name: 'click_count')
  int clickCount;

  @JsonKey(name: 'cur_price')
  String curPrice;
  @JsonKey(name: 'endTime')
  int endTime;

  @JsonKey(name: 'goods_id')
  int goodsId;

  @JsonKey(name: 'goods_name')
  String goodsName;
  @JsonKey(name: 'goods_status')
  int goodsStatus;
  @JsonKey(name: 'is_care')
  Is_care isCare;

  @JsonKey(name: 'original_img')
  String originalImg;

  @JsonKey(name: 'shop_price')
  String shopPrice;

  @JsonKey(name: 'start_price')
  String startPrice;


  ProListRes({
    this.goodsId,
    this.goodsName,
    this.careCount,
    this.clickCount,
    this.curPrice,
    this.endTime,
    this.goodsStatus,
    this.isCare,
    this.originalImg,
    this.shopPrice,
    this.startPrice
});

  factory ProListRes.fromJson(Map<String, dynamic> json) => _$ProListResFromJson(json);

  Map<String, dynamic> toJson() => _$ProListResToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}

@JsonSerializable()
class Is_care extends Object {

  @JsonKey(name: 'code')
  int code;

  @JsonKey(name: 'msg')
  String msg;

  Is_care(this.code,this.msg,);

  factory Is_care.fromJson(Map<String, dynamic> srcJson) => _$Is_careFromJson(srcJson);

  Map<String, dynamic> toJson() => _$Is_careToJson(this);

}