import 'package:json_annotation/json_annotation.dart';
part 'article_announcement.g.dart';
@JsonSerializable()
class ArticleAnnouncement {
  @JsonKey(name: 'add_time')
  int addTime;
  @JsonKey(name: 'article_id')
  int articleId;
  @JsonKey(name: 'author')
  String author;
  @JsonKey(name: 'click')
  int click;
  @JsonKey(name: 'description')
  String description;
  @JsonKey(name: 'publish_time')
  int publishTime;
  @JsonKey(name: 'thumb')
  String thumb;
  @JsonKey(name: 'title')
  String title;

  ArticleAnnouncement({this.addTime,this.articleId,this.author,this.click,this.description,this.publishTime,this.thumb,this.title});

  factory ArticleAnnouncement.fromJson(Map<String, dynamic> json) => _$ArticleAnnouncementFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleAnnouncementToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }

}