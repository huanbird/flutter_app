// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_article.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeArticle _$HomeArticleFromJson(Map<String, dynamic> json) {
  return HomeArticle(
    addTime: json['add_time'] as String,
    articleId: json['article_id'] as int,
    click: json['click'] as int,
    description: json['description'] as String,
    publishTime: json['publish_time'] as int,
    thumb: json['thumb'] as String,
    title: json['title'] as String,
  );
}

Map<String, dynamic> _$HomeArticleToJson(HomeArticle instance) =>
    <String, dynamic>{
      'add_time': instance.addTime,
      'article_id': instance.articleId,
      'click': instance.click,
      'description': instance.description,
      'publish_time': instance.publishTime,
      'thumb': instance.thumb,
      'title': instance.title,
    };
