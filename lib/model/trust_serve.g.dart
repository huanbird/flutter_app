// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trust_serve.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrustServe _$TrustServeFromJson(Map<String, dynamic> json) {
  return TrustServe(
    name: json['name'] as String,
    mobileName: json['mobile_name'] as String,
    image: json['image'] as String,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$TrustServeToJson(TrustServe instance) =>
    <String, dynamic>{
      'name': instance.name,
      'mobile_name': instance.mobileName,
      'image': instance.image,
      'id': instance.id,
    };
