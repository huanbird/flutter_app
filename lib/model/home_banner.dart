import 'package:json_annotation/json_annotation.dart';

part 'home_banner.g.dart';

@JsonSerializable()
class HomeBanner{
  @JsonKey(name: 'ad_code')
  String adCode;
  @JsonKey(name: 'ad_link')
  String adLink;

  HomeBanner({this.adCode, this.adLink});

  factory HomeBanner.fromJson(Map<String, dynamic> json) => _$HomeBannerFromJson(json);

  Map<String, dynamic> toJson() => _$HomeBannerToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }

}