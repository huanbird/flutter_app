import 'package:flutter/material.dart';
import 'package:device_apps/device_apps.dart';

class IsHaveWx with ChangeNotifier {
  bool isHwx = true;

  checkWx() async {
    isHwx = await DeviceApps.isAppInstalled('com.tencent.mm');
    notifyListeners();
  }
}