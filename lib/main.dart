import 'package:flutter/material.dart';
import 'package:flutter_renyi/index/index.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter_renyi/login/login.dart';
import 'package:flutter_renyi/provide/ishavewx.dart';
import './routers/routes.dart';
import './routers/application.dart';
import 'package:oktoast/oktoast.dart';
import 'package:provide/provide.dart';

void main() {
  var checkHaveWx = IsHaveWx();
  var providers = Providers();

  providers
    ..provide(Provider<IsHaveWx>.value(checkHaveWx));

  runApp(ProviderNode(child: MyApp(), providers: providers));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final router = Router();
    Routes.configureRoutes(router);
    Application.router = router;

    return OKToast(
      child: new MaterialApp(
        title: '人艺收藏',
        onGenerateRoute: Application.router.generator,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primaryColor: Color(0xffab2d28),
        ),
        //home: IndexPage(),
        home: Login(),
      ),
    );
  }
}
