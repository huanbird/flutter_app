
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

/// Toast工具类

const Color _bgColor = Colors.black87;
const double _radius = 3.0;

class Toast {
  static void show(String msg, {int duration = 2000}) {
    if (msg == null) {
      return;
    }
    showToast(
      msg,
      radius: 3.0,
      duration: Duration(milliseconds: duration),
      dismissOtherToast: true
    );
  }

  static void cancelToast() {
    dismissAllToast();
  }
}
