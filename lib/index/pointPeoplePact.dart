import 'package:flutter/material.dart';
import 'package:flutter_renyi/index/index_route.dart';
import 'package:flutter_renyi/routers/fluro_navigator.dart';

class PointPeoplePact extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text('协议'),),
      body: Container(
          child: SimpleDialog(
            children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 4, right: 4),
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            '指点迷津',
                            style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Text(
                            '1.每次只指点一件藏品，一次提交多件藏品照片不予指点。',
                            style:
                            TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          child: Text(
                            '2.纸币（邮票等纸质收藏品）需要上传正反两面照片和透光照片。',
                            style:
                            TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Text(
                            '3.银元、古钱币、邮票需要上传正反面照片和边齿照片。',
                            style:
                            TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Text(
                            '4.为方便藏品精准指点，请上传藏品多角度（前后、左右、上下）+局部细节高清照片；因上传图片模糊造成无法指点，指点费用不予退还。',
                            style:
                            TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Text(
                            '5.图片指点有一定局限性，指点结果仅作为行家与专家凭借个人经验、知识对提问者的有偿回答，指点结果仅供参考，不出鉴定证书不作为交易依据，人艺收藏网平台不对解答的完全正确性提供担保，也不为由此产生的某种因果关系承担责任。',
                            style:
                            TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Text(
                            '6.以上内容最终解释权归人艺收藏网所有',
                            style:
                            TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 50,
                          child: RaisedButton(
                            color: Color(0xffab2d28),
                            child: Text(
                              '我已知晓并同意次约定',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              NavigatorUtils.push(
                                  context, '${IndexRoute.pointPeoplePage}');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}