import 'package:flutter/material.dart';
import 'package:flutter_renyi/announcement/announcement.dart';
import 'package:flutter_renyi/index/home.dart';
import 'package:flutter_renyi/my/my.dart';
import 'package:flutter_renyi/push/push.dart';


class IndexPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _IndexState();
  }
}

class _IndexState extends State<IndexPage> {
  final List<BottomNavigationBarItem> bottomNavItems = [
    BottomNavigationBarItem(
      backgroundColor: Colors.black,
      icon: Icon(Icons.home),
      title: Text("首页",style: TextStyle(fontSize: 10),),
    ),
    BottomNavigationBarItem(
      backgroundColor: Colors.black,
      icon: Icon(Icons.view_headline),
      title: Text("公告"),
    ),
    BottomNavigationBarItem(
      backgroundColor: Colors.black,
      icon: Icon(Icons.open_in_browser),
      title: Text("发布"),
    ),
    BottomNavigationBarItem(
      backgroundColor: Colors.black,
      icon: Icon(Icons.person),
      title: Text("我的"),
    ),
  ];

  int currentIndex;

  final pages = [GetHome(), Announcement(), Push(), My()];

  @override
  void initState() {
    super.initState();
    currentIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text("底部导航栏"),
//      ),
      bottomNavigationBar: BottomNavigationBar(
        items: bottomNavItems,
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          _changePage(index);
        },
      ),
      body: pages[currentIndex],
    );
  }

  /*切换页面*/
  void _changePage(int index) {
    /*如果点击的导航项不是当前项  切换 */
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
      });
    }
  }
}
