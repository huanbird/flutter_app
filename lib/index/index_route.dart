import 'package:fluro/fluro.dart';
import 'package:flutter_renyi/index/buyerPage.dart';
import 'package:flutter_renyi/index/index.dart';
import 'package:flutter_renyi/index/pointPeoplePact.dart';
import 'package:flutter_renyi/index/pointPeoplePage.dart';
import 'package:flutter_renyi/index/trustPage.dart';
import '../routers/router_init.dart';



class IndexRoute implements IRouterProvider{

  static String trustPage = '/trustPage';

  static String buyerPage = '/buyerPage';

  static String pointPeoplePage = '/pointPeoplePage';

  static String pointPeoplePact = '/pointPeoplePact';

  static String homePage = '/home';


  @override
  void initRouter(Router router) {
    router.define(trustPage, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return TrustPage();
    }));
    router.define(buyerPage, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return BuyerPage();
    }));

    router.define(pointPeoplePage, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return PointPeolplePage();
    }));

    router.define(pointPeoplePact, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return PointPeoplePact();
    }));

    router.define(homePage, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return IndexPage();
    }));

  }

}