import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_renyi/api/apiRequest.dart';
import 'package:flutter_renyi/common/toast.dart';
import 'package:flutter_renyi/index/index_route.dart';
import 'package:flutter_renyi/list/pro_list_route.dart';
import 'package:flutter_renyi/model/get_help.dart';
import 'package:flutter_renyi/model/home_article.dart';
import 'package:flutter_renyi/model/home_banner.dart';
import 'package:flutter_renyi/model/is_commend.dart';
import 'package:flutter_renyi/model/trans_actions.dart';
import 'package:flutter_renyi/model/trust_serve.dart';
import 'package:flutter_renyi/routers/fluro_navigator.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:oktoast/oktoast.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../routers/application.dart';

class GetHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Home();
  }
}

class Home extends State<GetHome> {
  List<HomeBanner> banner = [];
  List<TransActions> _transActions;
  HomeArticle _article;
  IsCommend _is_commend;
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  TrustServe _trustServe;
  List<GetHelp> _getHelp;

  bool isReadPact = false;

  //下拉刷新
  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    await apiRequest
        .requestHome(
        '/homenew',
        method: apiRequest.GET
    ).then((value) {
      List res = value['top_banners'];
      List acres = value['jiaoyi'];
      List trustList = value['xiezhu'];
      setState(() {
        banner =
            res.map((dynamic json) => HomeBanner.fromJson(json)).toList();
        //_bannerLength = banner.length;
        _article = HomeArticle.fromJson(value['two_articles']);
        _is_commend = IsCommend.fromJson(value['yihai']);
        _transActions =
            acres.map((dynamic json) => TransActions.fromJson(json)).toList();
        _trustServe = TrustServe.fromJson(value['weituo']);
        _getHelp =
            trustList.map((dynamic json) => GetHelp.fromJson(json)).toList();
      });
      //print(banner.length);
    }).catchError((error) {
      print(error);
    });
    _refreshController.refreshCompleted();
  }

  //上来加载


  @override
  Widget build(BuildContext context) {
    //print('>> build <<');
    return Scaffold(
      appBar: AppBar(
        title: Text('首页'),
      ),
      body: FutureBuilder(
        future: apiRequest.requestHome('/homenew', method: apiRequest.GET),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var value = snapshot.data;
            List res = value['top_banners'];
            List acres = value['jiaoyi'];
            List trustList = value['xiezhu'];
            banner =
                res.map((dynamic json) => HomeBanner.fromJson(json)).toList();
            //_bannerLength = banner.length;
            _article = HomeArticle.fromJson(value['two_articles']);
            _is_commend = IsCommend.fromJson(value['yihai']);
            _transActions = acres
                .map((dynamic json) => TransActions.fromJson(json))
                .toList();
            _trustServe = TrustServe.fromJson(value['weituo']);
            _getHelp = trustList
                .map((dynamic json) => GetHelp.fromJson(json))
                .toList();
            return SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
              header: ClassicHeader(
                height: 45.0,
                releaseText: '松开手刷新',
                refreshingText: '刷新中',
                completeText: '刷新完成',
                failedText: '刷新失败',
                idleText: '下拉刷新',
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              //onLoading: _onLoading,
//              child: ListView.builder(
//                //padding: new EdgeInsets.all(5.0),
//                itemCount: _item.length,
//                itemBuilder: (BuildContext context, int index) {
//                  return buildListItem(_item[index]);
//                },
//              ),
              child: ListView(
                children: <Widget>[
                  TopSearch,
                  _bannerList(context, banner),
                  articleView(context, _article),
                  IsCommendview(context, _is_commend),
                  Transactionview(context, _transActions),

                  Trust(context, _trustServe),
                  Gethelp(context, _getHelp),
                  AboutRenyi(context),
                ],
              ),
            );
          } else {
            return Center(
              child: Text('加载中....'),
            );
          }
        },
      ),
    );
  }

//  Widget buildListItem(var object) {
//    print(object.runtimeType.toString());
//    if (object.runtimeType.toString() == "List<HomeBanner>") {
//      return _bannerList(context, object);
//    } else if (object.runtimeType.toString() == "HomeArticle") {
//      return articleView(context, object);
//    } else if (object.runtimeType.toString() == 'List<TransActions>') {
//      return Transactionview(context, object);
//    } else if (object.runtimeType.toString() == 'IsCommend') {
//      return IsCommendview(context, object);
//    } else if (object.runtimeType.toString() == 'TrustServe') {
//      return Trust(context, object);
//    } else if (object.runtimeType.toString() == 'List<GetHelp>') {
//      return Gethelp(context, object);
//    } else if (object.runtimeType.toString() == 'String') {
//      return AboutRenyi(context);
//    }
//  }

  Widget TopSearch = Container(
    padding: EdgeInsets.only(left: 13, right: 13),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              '人 艺',
              //this._setLogs,
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            Padding(padding: EdgeInsets.only(right: 10)),
            Text(
              'PEOPLE’S ART',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Icon(Icons.search),
            Icon(Icons.favorite_border),
          ],
        )
      ],
    ),
  );

  Widget _bannerList(BuildContext context, List<HomeBanner> banners) {
    return Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 300.0,
        child: Swiper(
          itemBuilder: (context, index) =>
              Image.network(
                '${banners[index]?.adCode ?? ''}',
                fit: BoxFit.fill,
              ),
          itemCount: banners.length,
          scrollDirection: Axis.horizontal,
          autoplay: true,
          onTap: (index) => print('点击了第$index个'),
        ));
  }

  Widget articleView(BuildContext context, HomeArticle homeArticle) {
    return Container(
      padding: EdgeInsets.only(left: 13, right: 13, top: 5, bottom: 15),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                homeArticle?.title ?? '',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
              ),
              Text(
                '10000+阅读',
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),
              ),
              Text(
                homeArticle?.title ?? '',
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    backgroundColor: Color(0xffab2d28),
                    color: Colors.white),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 8)),
          Row(
            children: <Widget>[
              Text(
                '纽约 | ${homeArticle?.addTime ?? ''}',
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w500,
                    color: Colors.black26),
              )
            ],
          )
        ],
      ),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))
//              border: Border.all(color: Colors.grey,width: 0.5),

//              borderRadius: BorderRadius.circular(5),
      ),
    );
  }

  Widget IsCommendview(BuildContext context, IsCommend _is_commend) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Ink(
            child: InkWell(
              onTap: (){
                NavigatorUtils.push(
                    context, '${ProListRoute.proListPage}?type=2');
              },
              child: Container(
                child: Image.network(_is_commend?.image ?? ''),
              ),
            ),
          ),

          Padding(padding: EdgeInsets.only(top: 10)),
          Text(
            _is_commend?.name ?? '',
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
          ),
          Text(
            '平台担保交易 | ${_is_commend?.addTime ?? ''}',
            style: TextStyle(fontWeight: FontWeight.w400, fontSize: 12),
          ),
        ],
      ),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))
//              border: Border.all(color: Colors.grey,width: 0.5),

//              borderRadius: BorderRadius.circular(5),
      ),
    );
  }

  Widget Transactionview(BuildContext context, List<TransActions> transAction) {
    print(transAction.length);
    return Container(
      padding: EdgeInsets.only(left: 13, right: 13, top: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '交易区',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
          ),
          ListView.builder(
            //padding: new EdgeInsets.all(5.0),
            shrinkWrap: true, //为true可以解决子控件必须设置高度的问题
            physics: new NeverScrollableScrollPhysics(), //禁用滑动事件
            itemCount: transAction.length,
            itemBuilder: (BuildContext context, int index) {
              return transItem(context, transAction[index], index);
            },
          ),
        ],
      ),
    );
  }

  Widget transItem(BuildContext context, TransActions transAction, int index) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 10)),
          new Ink(
            child: new InkWell(
              onTap: () {
                //print(index);
                if (index == 1) {
                  Toast.show('实名入住放心购');
                } else { //'${GoodsRouter.goodsEditPage}?isAdd=true&isScan=true'
                  NavigatorUtils.push(
                      context, '${ProListRoute.proListPage}?type=1');
                }
              },
              child: Container(
                child: Image.network(transAction.image),
              ),
            ),
          ),

          Padding(padding: EdgeInsets.only(bottom: 10)),
          Text(
            transAction.name,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
          ),
          Padding(padding: EdgeInsets.only(bottom: 8)),
          Row(
            children: <Widget>[
              Image.network(
                transAction.rightImage,
                width: 50,
                height: 40,
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Column(
                children: <Widget>[
                  Text(transAction.mobileName,
                      style: TextStyle(fontSize: 12, color: Colors.grey)),
                  Text(transAction.engIshName,
                      style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              SizedBox(
                width: 1,
                height: 25,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: Colors.grey),
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Text(transAction.catDesc,
                  style: TextStyle(fontSize: 12, color: Colors.grey))
            ],
          )
        ],
      ),
    );
  }

  Widget Trust(BuildContext context, TrustServe trustServe) {
    return Container(
      padding: EdgeInsets.only(left: 13, right: 13, top: 5, bottom: 5),
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            trustServe?.name ?? '',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w300),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Ink(
            child: InkWell(
              onTap: (){
                NavigatorUtils.push(
                    context, '${IndexRoute.trustPage}');
              },
              child: Image.network(trustServe?.image ?? ''),
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Text(
            'people"s art',
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
          ),
          Padding(padding: EdgeInsets.only(bottom: 8)),
          Row(
            children: <Widget>[
              Image.network(
                'http://wap.luckywen.com/static/img/_20190527232339.c90c203.jpg',
                width: 40,
                height: 40,
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Column(
                children: <Widget>[
                  Text(trustServe?.mobileName ?? '',
                      style: TextStyle(fontSize: 12, color: Colors.grey)),
                  //Text('(foreign)',style: TextStyle(fontSize: 12,color: Colors.grey))
                ],
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              SizedBox(
                width: 1,
                height: 25,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: Colors.grey),
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Column(
                children: <Widget>[
                  Text('您的需求，我的追求',
                      style: TextStyle(fontSize: 12, color: Colors.grey)),
                  Text('您的信任，我的动力',
                      style: TextStyle(fontSize: 12, color: Colors.grey)),
                ],
              ),
              // Padding(padding: EdgeInsets.only(left: 28)),
              Container(
                width: 70,
                height: 25,
                margin: EdgeInsets.only(left: 40),
                decoration: BoxDecoration(
                  border: new Border.all(
                    color: Colors.red, //边框颜色
                    width: 2, //边框宽度
                  ), // 边色与边宽度
                  color: Colors.grey, // 底色
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 0, //阴影范围
                      spreadRadius: 2, //阴影浓度
                      color: Colors.red, //阴影颜色
                    ),
                  ],
                  // borderRadius: BorderRadius.circular(10), // 圆角也可控件一边圆角大小
                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                  borderRadius: BorderRadius.only(
//                      topLeft: Radius.circular(5),
//                      topRight: Radius.circular(5),
//                      bottomLeft: Radius.circular(5),
//                      //bottomLeft : Radius.circular(10)),
//                      bottomRight: Radius.circular(5)), //单独加某一边圆角
                ),
                child: Column(
                  children: <Widget>[
                    Text(
                      "我要委托",
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      ),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))
//              border: Border.all(color: Colors.grey,width: 0.5),

//              borderRadius: BorderRadius.circular(5),
      ),
    );
  }

  Widget Gethelp(BuildContext context, List<GetHelp> _getHelp) {
    return Container(
      padding: EdgeInsets.only(left: 13, right: 13, top: 5, bottom: 5),
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '为阁下提供各方协助',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Ink(
            child: InkWell(
              onTap: (){
                NavigatorUtils.push(
                    context, '${IndexRoute.buyerPage}');
              },
              child:Image.network('http://wap.luckywen.com/static/img/mjq.7977008.jpg'),
            )
          ),

          Padding(padding: EdgeInsets.only(top: 10)),

          Ink(
              child: InkWell(
                onTap: (){
                  NavigatorUtils.push(
                      context, '${IndexRoute.pointPeoplePact}');

                },
                child:Image.network('http://wap.luckywen.com/static/img/zdmj.bb31f36.jpg'),
              )
          ),


          Padding(padding: EdgeInsets.only(top: 10)),
          Image.network('http://wap.luckywen.com/static/img/kscj.ef9810a.jpg'),
          Padding(padding: EdgeInsets.only(top: 10)),
          Image.network('http://wap.luckywen.com/static/img/scj.2840f5d.jpg'),
        ],
      ),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))
//              border: Border.all(color: Colors.grey,width: 0.5),

//              borderRadius: BorderRadius.circular(5),
      ),
    );
  }

//  Widget trustItem(BuildContext context,GetHelp _getHelp){
//    return Container(
//      child: Column(
//        children: <Widget>[
//          Padding(padding: EdgeInsets.only(top: 10)),
//          Image.network(_getHelp?.image ?? ''),
//        ],
//      ),
//    );
//  }
//
  Widget AboutRenyi(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 13, right: 13, top: 5, bottom: 5),
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '关于人艺',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          new InkWell(
              onTap: () {
                Application.router.navigateTo(context, "/about_renyi");
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.network(
                      'http://wap.luckywen.com/static/img/_20190527232339.c90c203.jpg'),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Text(
                    '关于人艺更多信息',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
