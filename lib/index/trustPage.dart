import 'package:flutter/material.dart';

class TrustPage extends StatelessWidget{
  @override

  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('委托服务'),
      ),
      body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Image.network('http://wap.luckywen.com/static/img/20190527235948_02.cfe1600.jpg'),
              ),
              
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 12,),
                    Text('人艺收藏委托服务',textAlign: TextAlign.center,style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600),),
                    SizedBox(height: 12,),
                    Text('人艺收藏为适应广大用户需求，让艺术服务于人民',textAlign: TextAlign.center),
                    SizedBox(height: 12,),
                    Text('现为广大的藏友开通送拍服务！具体要求如下。',textAlign: TextAlign.center),
                    SizedBox(height: 12,),
                    Text('送拍服务有用户向平台提供相关材料，由平台通过官方渠道向拍企递交商品材料的服务，送拍原则：平台不保证商品一定被拍企接受。平台只负责递送材料，具体事宜以拍企为准。',textAlign: TextAlign.center),
                    SizedBox(height: 12,),
                    Text('详情请参考<人艺送拍服务规则>',textAlign: TextAlign.center),
                    SizedBox(height: 12,),
                    //Text('委托藏品',textAlign: TextAlign.center),
                    Container(
                      width: 110,
                      height: 50,
                      child: RaisedButton(
                          color: Color(0xffc33000),
                        child: Text('委托藏品',style: TextStyle(color: Colors.white,fontSize: 19),),
                          onPressed: () {
                          }),
                      ),
                    SizedBox(height: 12,),
                    Text('《中国文物艺术品拍卖企业自律公约》成员单位',textAlign: TextAlign.center,style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),

                    Container(
                      child: Image.network('http://wap.luckywen.com/static/img/_20190515215416.02765dd.jpg'),
                    ),
                    SizedBox(height: 12,),
                    Container(
                      margin: EdgeInsets.only(left: 20,right: 20),
                      child: Text(
                          '注：平台只负责官方渠道递送征集材料不保证企业一定接受采访。详细操作流程以拍企为准！',
                          textAlign: TextAlign.center,
                        style: TextStyle(color: Color(0xffc63000)),
                      ),
                    ),

                  ],
                ),
              ),


            ],
          ),
      ),
    );
  }


}