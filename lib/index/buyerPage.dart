import 'package:flutter/material.dart';

class BuyerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(110),
        child: Column(
          children: [
            AppBar(
              title: Text('买家区'),
            ),
            Container(
              //color: Colors.red,
              height: 50,
              padding: EdgeInsets.only(left: 13, right: 13),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        '人 艺',
                        //this._setLogs,
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.w600),
                      ),
                      Padding(padding: EdgeInsets.only(right: 10)),
                      Text(
                        'PEOPLE’S ART',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.search),
                      Icon(Icons.favorite_border),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Image.network(
                  'http://wap.luckywen.com/static/img/_20190527235627.5ae97e0.jpg'),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              margin: EdgeInsets.only(left: 11, bottom: 11),
              alignment: Alignment.centerLeft,
              child: Text(
                '简介',
                style: TextStyle(
                    fontSize: 30,
                    color: Color(0xffab2d28),
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 14, right: 14),
              child: Text(
                '        人艺收藏为资深藏家，广大买家，和初次购置画作或其他艺术品的人士，'
                '提供专属的私人服务。通过这项特殊的【度身洽购】服务，'
                '人艺收藏协助买家以固定的价格搜罗特定的藏品，并以中间人的身份安排保密交易，'
                '且不受常规限制，协助您随时不论您处于何种地域，何种范畴，是否有存在时差所限，'
                '助人艺收藏平台买家随时觅得心头好。',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              margin: EdgeInsets.only(left: 14, right: 14),
              child: Text(
                '人艺收藏将就私人洽购策略和方向，为平台客户提供个别的专业意见。',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              margin: EdgeInsets.only(left: 14, right: 14),
              child: Text(
                '如您欲了解人艺收藏的度身服务，请及时与您专属指导顾问联系，或平台咨询',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OutlineButton(
                  color: Colors.white,
                  borderSide: BorderSide(color: Colors.black,width: 2),
                  onPressed: () {},
                  child: Text('度身洽谈',style: TextStyle(color: Color(0xffab2d28))),
                ),
                SizedBox(width: 30,),
                OutlineButton(
                  color: Colors.white,
                  borderSide: BorderSide(color: Colors.black,width: 2),
                  onPressed: () {},
                  child: Text('买家登陆',style: TextStyle(color: Color(0xffab2d28)),),
                )
              ],
            ),
            SizedBox(height: 50,),
          ],
        ),
      ),
    );
  }
}
