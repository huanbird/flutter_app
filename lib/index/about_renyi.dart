import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutRenYi extends StatelessWidget {
  //final String goodsId;
  //DetailsPage(this.goodsId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('关于人艺'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            title(context),
            desc(context),
            culture(context),
            threeSlgan(context),
            bottomDesc(context),
            bottomFourSlgan(context),
          ],
        ),
      ),
    );
  }

  Widget title(BuildContext context) {
    return Container(
      //width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 45, bottom: 20),
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Divider(
              height: 0.0,
              indent: 135,
              endIndent: 135,
              color: Color(0xffa62220)),
          Padding(padding: EdgeInsets.only(top: 18)),
          Text(
            'ABOUT US',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 24,
            ),
          ),
          Text(
            '关于人艺',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
            ),
          )
        ],
      ),
    );
  }

  Widget desc(BuildContext context) {
    return Container(
      //width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 10, right: 5, left: 5),
      child: Wrap(
        spacing: 8.0,
        runSpacing: 8.0,
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(
                '一站式专业服务平台',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 5, bottom: 8)),
              Text(
                '\t\t\t\t\t\t\t\t人艺收藏致力于促进艺术品行业的发展，创造艺术品交易的新格局，'
                '开创首家在线电子商务竞拍平台，'
                '近五年的技术研发团队，结合“互联网+”生态战略，不忘初心，弘扬传统文化，'
                '为藏家提供绿色健康的艺术品生态交易圈。',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                  fontSize: 15,
                ),
              ),
              Padding(padding: EdgeInsets.only(bottom: 2)),
              Text(
                '\t\t\t\t\t\t\t\t经过Internet Content Provider的审核备案，'
                '工商行政管理相关部门严格的审核和帮助之下，人艺收藏全体工作人员的不断努力，'
                '使我们不仅仅拥有国际域名，同时“人艺收藏”不忘初心，'
                '永铸中国梦的思想深深影响着整个行业以及收藏爱好者，'
                '其企业形象不仅的到了极大的认可，'
                '诚信为本的经营方式也得到行业以及社会知名人士的赞誉。',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                  fontSize: 15,
                ),
              ),
              Padding(padding: EdgeInsets.only(bottom: 8)),
            ],
          ),
        ],
      ),
      decoration: new BoxDecoration(
        color: Color(0xFFa62220),
        //borderRadius: BorderRadius.all(radius),
      ),
    );
  }

  Widget culture(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30, bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Divider(
              height: 0.0,
              indent: 135,
              endIndent: 135,
              color: Color(0xffa62220)),
          Padding(padding: EdgeInsets.only(top: 10)),
          Text(
            '品牌文化',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 24,
            ),
          ),
        ],
      ),
    );
  }

  Widget threeSlgan(BuildContext context) {
    return Container(
      //width: MediaQuery.of(context).size.width,
      child: Wrap(
        alignment: WrapAlignment.spaceAround,
        spacing: 38,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: 70,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: new Border.all(
                    color: Colors.grey, //边框颜色
                    width: 0.1, //边框宽度
                  ), // 边色与边宽度
                  color: Colors.white, // 底色
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 0.2, //阴影范围
                      spreadRadius: 0.1, //阴影浓度
                      color: Colors.grey, //阴影颜色
                    ),
                  ],
                  // borderRadius: BorderRadius.circular(10), // 圆角也可控件一边圆角大小
                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                  borderRadius: BorderRadius.only(
//                      topLeft: Radius.circular(5),
//                      topRight: Radius.circular(5),
//                      bottomLeft: Radius.circular(5),
//                      //bottomLeft : Radius.circular(10)),
//                      bottomRight: Radius.circular(5)), //单独加某一边圆角
                ),
                child: Text('使命'),
              ),
              Text('为人民服务'),
              Text('一键即达'),
              Text('一键即达'),
            ],
          ),
          Column(
            children: <Widget>[
              //Padding(padding: EdgeInsets.only(left: 20)),
              Container(
                margin: EdgeInsets.only(left: 28),
                width: 70,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: new Border.all(
                    color: Colors.grey, //边框颜色
                    width: 0.1, //边框宽度
                  ), // 边色与边宽度
                  color: Colors.white, // 底色
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 0.2, //阴影范围
                      spreadRadius: 0.1, //阴影浓度
                      color: Colors.grey, //阴影颜色
                    ),
                  ],
                  // borderRadius: BorderRadius.circular(10), // 圆角也可控件一边圆角大小
                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                  borderRadius: BorderRadius.only(
//                      topLeft: Radius.circular(5),
//                      topRight: Radius.circular(5),
//                      bottomLeft: Radius.circular(5),
//                      //bottomLeft : Radius.circular(10)),
//                      bottomRight: Radius.circular(5)), //单独加某一边圆角
                ),
                child: Text('价值观'),
              ),
              Text('\t\t\t\t\t\t\t\t诚实、 务实'),
              Text('\t\t\t\t\t\t\t\t公平公正'),
              Text('\t\t\t\t\t\t\t\t公开'),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: 70,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: new Border.all(
                    color: Colors.grey, //边框颜色
                    width: 0.1, //边框宽度
                  ), // 边色与边宽度
                  color: Colors.white, // 底色
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 0.2, //阴影范围
                      spreadRadius: 0.1, //阴影浓度
                      color: Colors.grey, //阴影颜色
                    ),
                  ],
                  // borderRadius: BorderRadius.circular(10), // 圆角也可控件一边圆角大小
                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                  borderRadius: BorderRadius.only(
//                      topLeft: Radius.circular(5),
//                      topRight: Radius.circular(5),
//                      bottomLeft: Radius.circular(5),
//                      //bottomLeft : Radius.circular(10)),
//                      bottomRight: Radius.circular(5)), //单独加某一边圆角
                ),
                child: Text('愿景'),
              ),
              Text('打造绿色艺术'),
              Text('交易生态圈'),
              Text('让文化发展'),
              Text('走进生活'),
              Text('让艺术流行起来'),
            ],
          ),
        ],
      ),
    );
  }

  Widget bottomDesc(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 30)),
          Divider(
              height: 0.0,
              indent: 135,
              endIndent: 135,
              color: Color(0xffa62220)),
          Padding(padding: EdgeInsets.only(bottom: 20)),
          Text(
            '人艺收藏独家"4in1"指导系统',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          Padding(padding: EdgeInsets.only(bottom: 25)),
        ],
      ),
    );
  }

  Widget bottomFourSlgan(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Wrap(
        alignment: WrapAlignment.spaceAround,
        spacing: 15,
        children: <Widget>[
          for (Map item in Tags) TagItem(item)
        ],
      ),
    );
  }
}

const List<Map<String,dynamic>> Tags = [
  {
    "title" : "1.藏品把关",
    "desc" : "人艺收藏专业人员对入选平台的藏品持有者以及藏品性进行严格把关",
    "backColor" : 0xFFe79e7d,
  },
  {
    "title" : "2.细心教导",
    "desc" : "专属老师对入选藏品进行细化分析，以及上传指导。",
    "backColor" : 0xFF86A9E1,
  },
  {
    "title" : "3.藏品把关",
    "desc" : "定期追踪藏品情况，对比阶段性目标调整目标和方案。",
    "backColor" : 0xFF476BA7,
  },
  {
    "title" : "4.效果反馈",
    "desc" : "给与藏品全程一站式动向，提供二次服务。",
    "backColor" : 0xFFC0B5B1,
  },
];

class TagItem extends StatelessWidget {
  final Map text;
  TagItem(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 155,
      height: 90,
      margin: EdgeInsets.only(top: 15),
      child: Column(
        children: <Widget>[
          Text(this.text['title'].toString(), style: TextStyle(fontSize: 14, color: Colors.white)),
          Text(this.text['desc'].toString(),
              style: TextStyle(fontSize: 14, color: Colors.white)),
        ],
      ),
      decoration: new BoxDecoration(
        color: Color(this.text['backColor']),
        //borderRadius: BorderRadius.all(radius),
      ),
    );
  }
}
