import 'package:flutter/material.dart';
import 'package:flutter_renyi/common/toast.dart';
import 'package:flutter_renyi/index/index_route.dart';
import 'package:flutter_renyi/routers/fluro_navigator.dart';
import 'dart:async';

import 'package:multi_image_picker/multi_image_picker.dart';

class PointPeolplePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PointPeolple();
  }
}

class PointPeolple extends State<PointPeolplePage> {
  bool isHide = false;

  List<Asset> images = List<Asset>();
  String _error;
  int choseLength = 0;
  int iniLength = 6;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            title: Text('指点迷津'),
          ),
          body: ListView(
            children: [

              //开始布局指点迷津
              SizedBox(height: 5,),
              // Expanded(
              //   child: buildGridView(),
              // ),

              Container(
                margin: EdgeInsets.only(left: 10,top: 10,bottom: 10),
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  border: new Border.all(
                    //color: Colors.red, //边框颜色
                    width: 1, //边框宽度
                  ), // 边色与边宽度
                  // borderRadius: BorderRadius.circular(10), // 圆角也可控件一边圆角大小
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Column(
                  children: [

                    Ink(
                      child: InkWell(
                        onTap: loadAssets,
                        child: Container(
                          width : 70,
                          height: 70,
                          child: Icon(Icons.add,size: 70,),
                        ),
                      ),
                    ),
                    Container(
                      child: Text('上传图片',style: TextStyle(color: Color(0xffab2d28)),),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 5,),
              Container(
                color: Colors.red,
                child: Text('至少2张，最多6张，请上传前后左右上下'),
              ),
            ],
          ),
        ),
        onWillPop: (){
          NavigatorUtils.goBackWithPage(
              context, '${IndexRoute.homePage}');
        }
    );
  }

  Widget buildGridView() {
    if (images != null)
      return GridView.count(
        crossAxisSpacing:3,
        mainAxisSpacing : 5,
        crossAxisCount: 4,
        children: List.generate(images.length, (index) {
          Asset asset = images[index];
          return AssetThumb(
            asset: asset,
            width: 100,
            height: 100,
          );
        }),
      );
    else{
      return Container(color: Colors.white,height: 100,);
    }

  }

  Future<void> loadAssets() async {
    // setState(() {
    //   images = List<Asset>();
    // });

    List<Asset> resultList;
    String error;
    try {
      if((iniLength - choseLength) == 0){
        return Toast.show('图片不能多余6涨哦');
      }
      resultList = await MultiImagePicker.pickImages(
        maxImages: iniLength - choseLength,
        enableCamera: true,
        materialOptions: MaterialOptions(
          actionBarTitle: "Action bar",
          allViewTitle: "全部图片",
          actionBarColor: "#ffab2d28",
          actionBarTitleColor: "#000000",
          lightStatusBar: true,
          statusBarColor: '#ffab2d28',
          startInAllView: true,
          selectCircleStrokeColor: "#ffab2d28",
          selectionLimitReachedText: "你已经不能够选择啦",
        ),
      );
    } on Exception catch (e) {

      error = e.toString();
      print(error);
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images.addAll(resultList);
      choseLength = images.length;
      if (error == null) _error = '你什么也没有选';
    });

    print('-------');
    print(choseLength);
    print(images.length);
    print('-------');
  }
}
