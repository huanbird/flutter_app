import 'package:flutter/material.dart';
import 'package:flutter_renyi/index/index_route.dart';
import 'package:flutter_renyi/list/pro_list_route.dart';
import 'package:flutter_renyi/login/login_route.dart';
import './404.dart';
import 'package:fluro/fluro.dart';
import '../index/about_renyi.dart';
import './router_init.dart';

class Routes{
  static String root='/';
  static String aboutRrnYiPage = '/about_renyi';
  static final List<IRouterProvider> _listRouter = [];

  static void configureRoutes(Router router) {
    /// 指定路由跳转错误返回页
    router.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          debugPrint('未找到目标页');
          return PageNotFound();
        });
    router.define(aboutRrnYiPage, handler: Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) => AboutRenYi()));

//    router.define(webViewPage, handler: Handler(handlerFunc: (_, params) {
//      final String title = params['title']?.first;
//      final String url = params['url']?.first;
//      return WebViewPage(title: title, url: url);
//    }));

    _listRouter.clear();
    /// 各自路由由各自模块管理，统一在此添加初始化
    _listRouter.add(ProListRoute());
    _listRouter.add(IndexRoute());
    _listRouter.add(LoginRoute());
//    _listRouter.add(OrderRouter());
//    _listRouter.add(StoreRouter());
//    _listRouter.add(AccountRouter());
//    _listRouter.add(SettingRouter());
//    _listRouter.add(StatisticsRouter());

    /// 初始化路由
    _listRouter.forEach((routerProvider) {
      routerProvider.initRouter(router);
    });
  }

}