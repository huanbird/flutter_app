import 'package:flutter/material.dart';

class Push extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new PushState();
  }
}

class PushState extends State<Push>{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('发布'),
      ),
      body: new Center(
        child: Icon(Icons.mood,size: 130.0,color: Colors.blue,),
      ),
    );
  }
}