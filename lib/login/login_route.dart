import 'package:fluro/fluro.dart';
import 'package:flutter_renyi/login/login.dart';
import 'package:flutter_renyi/login/send_login.dart';
import '../routers/router_init.dart';



class LoginRoute implements IRouterProvider{

  static String loginPage = '/login';
  static String sendLoginPage = '/sendLogin';

  @override
  void initRouter(Router router) {
    router.define(loginPage, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return Login();
    }));

    router.define(sendLoginPage, handler: Handler(handlerFunc: (_, params) {
      //final int index = int.parse();
      return SendLogin();
    }));
  }

}