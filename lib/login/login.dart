import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_renyi/login/login_route.dart';
import 'package:flutter_renyi/provide/ishavewx.dart';
import 'package:flutter_renyi/routers/fluro_navigator.dart';
import 'package:provide/provide.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffab2d28),
      body: FutureBuilder(
        future: _getBackInfo(context),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Offstage(
                    offstage:
                        Provide.value<IsHaveWx>(context).isHwx ? false : true,
                    child: Container(
                      width: 180,
                      height: 50,
                      child: RaisedButton(
                        color: Colors.white,
                        splashColor: Color(0xffab2d28),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.red),
                            borderRadius: BorderRadius.circular(40)),
                        child: Text(
                          '微信一键登陆',
                          style: TextStyle(fontSize: 17, color: Colors.black),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    width: 180,
                    height: 50,
                    child: RaisedButton(
                      splashColor: Color(0xffab2d28),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.red),
                          borderRadius: BorderRadius.circular(40)),
                      child: Text(
                        '手机号码登陆',
                        style: TextStyle(fontSize: 17, color: Colors.black),
                      ),
                      onPressed: () {
                        NavigatorUtils.push(
                            context, '${LoginRoute.sendLoginPage}',clearStack : true);
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          return Text('请稍后');
        },
      ),
    );
  }

  Future _getBackInfo(BuildContext context) async {
    await Provide.value<IsHaveWx>(context).checkWx();
    return true;
  }
}
