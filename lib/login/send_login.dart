import 'dart:async';

import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_renyi/common/toast.dart';
import 'package:flutter_renyi/index/index_route.dart';
import 'package:flutter_renyi/routers/fluro_navigator.dart';

class SendLogin extends StatefulWidget {
  @override
  _Login createState() => new _Login();
}

class _Login extends State<SendLogin> {
  //获取Key用来获取Form表单组件
  GlobalKey<FormState> loginKey = new GlobalKey<FormState>();
  String userName = '';
  String password = '';
  bool isShowPassWord = false;

  bool  isButtonEnable=true;      //按钮状态  是否可点击
  String buttonText='发送验证码';   //初始文本
  int count=60;                     //初始倒计时时间
  Timer timer;

  void login(){
    //读取当前的Form状态
    var loginForm = loginKey.currentState;
    //验证Form表单
    if(loginForm.validate()){
      loginForm.save();
      if(userName.length == 11 && password.length == 6){
        print('开始登陆');
        NavigatorUtils.push(
            context, '${IndexRoute.homePage}',clearStack : true);
      }else{
        print('输入错误: ' + userName + ' password: ' + password);
      }

    }
  }



  void _buttonClickListen(){
    setState(() {
      if(isButtonEnable){         //当按钮可点击时
        isButtonEnable=false;   //按钮状态标记
        _initTimer();

        return null;            //返回null按钮禁止点击
      }else{                    //当按钮不可点击时
//        debugPrint('false');
        return null;             //返回null按钮禁止点击
      }
    });
  }

  void _initTimer(){
    timer = new Timer.periodic(Duration(seconds: 1), (Timer timer) {
      count--;
      setState(() {
        if(count==0){
          timer.cancel();             //倒计时结束取消定时器
          isButtonEnable=true;        //按钮可点击
          count=60;                   //重置时间
          buttonText='发送验证码';     //重置按钮文本
        }else{
          buttonText='重新发送($count)';  //更新文本内容
        }
      });
    });
  }

  @override
  void dispose() {
    timer?.cancel();      //销毁计时器
    timer=null;
    //mController?.dispose();
    //   SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    return new MaterialApp(
      title: 'Form表单示例',
      builder: (context, child) => Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus.unfocus();
            }
          },
          child: child,
        ),
      ),
      home: new Scaffold(
        body: new Column(
          children: <Widget>[
            new Container(
                padding: EdgeInsets.only(top: 100.0, bottom: 10.0),
                child: new Text(
                  'LOGO',
                  style: TextStyle(
                      color: Color.fromARGB(255, 53, 53, 53),
                      fontSize: 50.0
                  ),
                )
            ),
            new Container(
              padding: const EdgeInsets.all(16.0),
              child: new Form(
                key: loginKey,
                autovalidate: true,
                child: new Column(
                  children: <Widget>[
                    new Container(
                      child: new TextFormField(
                        //maxLength: 11,
                        decoration: new InputDecoration(
                          labelText: '请输入手机号',
                          labelStyle: new TextStyle( fontSize: 15.0, color: Color.fromARGB(255, 93, 93, 93)),
                          //border: Border(),
                          // suffixIcon: new IconButton(
                          //   icon: new Icon(
                          //     Icons.close,
                          //     color: Color.fromARGB(255, 126, 126, 126),
                          //   ),
                          //   onPressed: () {

                          //   },
                          // ),
                        ),
                        keyboardType: TextInputType.phone,
                        onSaved: (value) {
                          if(!RegexUtil.isMobileSimple(value)){
                            Toast.show('手机号不正确');
                            userName = '';
                          }else{
                            userName = value;
                          }

                        },
                        validator: (phone) {
                          if(phone.length == 0){
                            return '请输入手机号';
                          }
                        },
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 4,
                          child: new Container(
                            padding: EdgeInsets.only(right: 10),
                            child: new TextFormField(
                            //maxLength: 6,
                            decoration:  new InputDecoration(
                              labelText: '请输入6位验证码',
                              labelStyle: new TextStyle( fontSize: 15.0, color: Color.fromARGB(255, 93, 93, 93)),
                            ),
                            keyboardType: TextInputType.number,
                            //obscureText: !isShowPassWord,
                            onSaved: (value) {
                              if(!RegexUtil.matches(r"^\d{6}$", value)){
                                Toast.show('验证码不正确');
                                password = '';
                              }else{
                                password = value;
                              }

                            },
                          ),
                        ),
                        ),
                        Expanded(
                          flex: 2,
                          child:Container(
                            padding: EdgeInsets.only(left: 0),
                            child: FlatButton(
                              disabledColor: Colors.grey.withOpacity(0.1),     //按钮禁用时的颜色
                              disabledTextColor: Colors.white,                   //按钮禁用时的文本颜色
                              textColor:isButtonEnable?Colors.white:Colors.black.withOpacity(0.2),                           //文本颜色
                              color: isButtonEnable?Color(0xff44c5fe):Colors.grey.withOpacity(0.1),                          //按钮的颜色
                              splashColor: isButtonEnable?Colors.white.withOpacity(0.1):Colors.transparent,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              onPressed: (){ setState(() {
                                _buttonClickListen();
                              });},
//                        child: Text('重新发送 (${secondSy})'),
                              child: Text('$buttonText',style: TextStyle(fontSize: 13,),),
                            ),
                          ),
                        ),
                      ],
                    ),

                    new Container(
                      height: 45.0,
                      margin: EdgeInsets.only(top: 40.0),
                      child: new SizedBox.expand(
                        child: new RaisedButton(
                          onPressed: login,
                          color: Color.fromARGB(255, 61, 203, 128),
                          child: new Text(
                            '登录',
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Color.fromARGB(255, 255, 255, 255)
                            ),
                          ),
                          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(45.0)),
                        ),
                      ),
                    ),
                    // new Container(
                    //   margin: EdgeInsets.only(top: 30.0),
                    //   padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    //   child: new Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: <Widget>[
                    //       new Container(
                    //         child:  Text(
                    //           '注册账号',
                    //           style: TextStyle(
                    //               fontSize: 13.0,
                    //               color: Color.fromARGB(255, 53, 53, 53)
                    //           ),
                    //         ),
                    //
                    //       ),
                    //
                    //       Text(
                    //         '忘记密码？',
                    //         style: TextStyle(
                    //             fontSize: 13.0,
                    //             color: Color.fromARGB(255, 53, 53, 53)
                    //         ),
                    //       ),
                    //     ],
                    //   ) ,
                    // ),

                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}