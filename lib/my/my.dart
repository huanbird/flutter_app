import 'package:flutter/material.dart';

class My extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new MyState();
  }
}

class MyState extends State<My>{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('我的'),
      ),
      body: new Center(
        child: Icon(Icons.mood,size: 130.0,color: Colors.blue,),
      ),
    );
  }
}